<?php

session_start();

require "./connection.php";

if(!isset($_SESSION['user_id'])){
	header('location: ../views/login.php');
}else{
	if(isset($_SESSION['cart'])){
		$user_id = $_SESSION['user_id'];
		$total = 0;

		$sql = "INSERT INTO orders(user_id) VALUES ($user_id)";
		mysqli_query($conn, $sql) or die(mysqli_error($conn));
		$order_id = mysqli_insert_id($conn);

		//iterate over the cart session variable, creating records in the products_orders pivot table
		//for every product iterated over
		foreach($_SESSION['cart'] as $id => $quantity){
			$sql = "SELECT * FROM products WHERE id = '$id'";
			$product = mysqli_fetch_assoc(mysqli_query($conn, $sql));
			$subtotal =$product['price'] * $quantity;
			$total += $subtotal;

			$sql = "INSERT INTO products_orders (order_id, prod_id, quantity, subtotal) VALUES ('$order_id', '$id', '$quantity', '$subtotal')";
			mysqli_query($conn, $sql) or die(mysqli_error($conn));
		}
		$sql = "UPDATE orders SET total = '$total' WHERE id = '$order_id'";
		mysqli_query($conn, $sql) or die(mysqli_error($conn));
		unset($_SESSION['cart']);
		header('location: ../views/products_catalog.php');
	}
}