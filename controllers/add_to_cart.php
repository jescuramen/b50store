<?php

session_start();

//echo "Id is {$_POST['id']}.";
//echo "Quantity is {$_POST['quantity']}.";

$id = $_POST['id'];
$quantity = $_POST['quantity'];

//if the product id is already set in the session variable named cart
if(isset($_SESSION['cart'][$id])){
	//add the new quantity to the existing quantity
	$_SESSION['cart'][$id] += $quantity;
}else{//product id is not yet in the session cart
	//set the id with value eqaul to quantity in the session variable cart
	$_SESSION['cart'][$id] = $quantity;
}