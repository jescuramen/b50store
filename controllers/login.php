<?php

session_start();
require "connection.php";

$username = htmlspecialchars($_POST['username']);
$password = sha1(htmlspecialchars($_POST['password']));

$sql = "SELECT * FROM users WHERE username = '$username' AND password = '$password'";

//this returns a database row
$result = mysqli_query($conn, $sql);

//check if we have a match in the users table
if(mysqli_num_rows($result) === 1){
	//convert the row returned by mysqli_query into a PHP associative array
	$user = mysqli_fetch_assoc($result);
	//save as session variables the user information that we want to persist across the entire web app
	//we do so by using the PHP superglobal $_SESSION['desired_key_name'];
	$_SESSION['user_id'] = $user['id'];
	$_SESSION['username'] = $user['username'];
	$_SESSION['isAdmin'] = $user['isAdmin'];
	if($_SESSION['isAdmin'] == 1){
		header('location: ../views/products_dashboard.php');
	}else{
		header('location: ../views/products_catalog.php');
	}
}else{
	echo "Login failed. <br> <a href='../views/login.php'>Login</a> again.";
}