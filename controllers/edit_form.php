<?php

require "connection.php";
//product id
$id = htmlspecialchars($_GET['id']);

$queryProduct = "SELECT * FROM products WHERE id=$id" ;
$product = mysqli_fetch_assoc(mysqli_query($conn, $queryProduct));

$queryCategories = "SELECT * FROM categories";
$categories = mysqli_query($conn, $queryCategories) or die(mysqli_error($conn));

?>

<form method="POST" action="../controllers/edit_product.php?id=<?= $id; ?>" enctype="multipart/form-data">
	<div class="form-group">
		<label for="name">Name:</label>
		<input class="form-control" type="text" id="name" name="name" value="<?= $product['name']; ?>">
	</div>
	<div class="form-group">
		<label for="description">Description:</label>
		<input class="form-control" type="text" id="description" name="description" value="<?= $product['description']; ?>">
	</div>
	<div class="form-group">
		<label for="price">Price:</label>
		<input class="form-control" type="number" id="price" name="price" value="<?= $product['price']; ?>">
	</div>
	<!-- category select dropdown -->
	<div class="form-group">
		<label for="category_id">Category:</label>
		<select class="form-control" id="category_id" name="category">
			<option>Select a category:</option>
			<?php foreach ($categories as $category) : ?>
				<?php if($category['id'] == $product['category_id']) : ?>
					<option selected value="<?= $category['id']; ?>">
						<?= $category['name']; ?>
					</option>
				<?php else: ?>
					<option value="<?= $category['id']; ?>">
						<?= $category['name']; ?>
					</option>
				<?php endif; ?>
			<?php endforeach; ?>
		</select>
	</div>
	<div class="form-group">
		<label for="image">Image:</label>
		<input class="form-control" type="file" id="image" name="image">
	</div>
	<button class="btn btn-success">Edit Product</button>
</form>