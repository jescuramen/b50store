<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="./products_catalog.php">B50Store</a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar">
    <!-- burger icon -->
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- collapsible navigation menu -->
  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav ml-auto">
      <!-- inject php here to apply conditional rendering of navigation menu items -->
      <?php
        //validate if no user is logged in
        if(!isset($_SESSION['user_id'])){?>
          <li class="nav-item">
            <a class="nav-link" href="./products_catalog.php">Catalog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./cart.php">Cart
              <span id="cartCount"><?php
                if(isset($_SESSION['cart']) && count($_SESSION['cart'])){
                  echo array_sum($_SESSION['cart']);
                }?></span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./login.php">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./register.php">Register</a>
          </li>
        <?php }else{
          if($_SESSION['isAdmin'] == 0){ ?>
            <li class="nav-item">
              <a class="nav-link" href="./products_catalog.php">Catalog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./cart.php">Cart
                <span id="cartCount"><?php
                  if(isset($_SESSION['cart']) && count($_SESSION['cart'])){
                    echo array_sum($_SESSION['cart']);
                  }?></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../controllers/logout.php">Logout</a>
            </li>
          <?php }else{ ?>
            <li class="nav-item">
              <aclass="nav-link" href="./products_dashboard.php">Products Dash</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../controllers/logout.php">Logout</a>
            </li>
          <?php }
        }
      ?>
    </ul>
  </div>
</nav>