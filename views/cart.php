<?php

session_start();

if(isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1){
	header('location: ./products_dashboard.php');
}else{
	$title = "My Cart";

	function get_content(){
		require "../controllers/connection.php";

		//if the cart is set and is NOT empty
		if(isset($_SESSION['cart']) && count($_SESSION['cart'])) :
		?>
			<table class="table">
				<thead>
					<tr>
						<th>Product</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Subtotal</th>
					</tr>
				</thead>

				<tbody>
					<?php
					$total = 0;
					foreach($_SESSION['cart'] as $id => $quantity) :
						$query = "SELECT * FROM products WHERE id = '$id'";
						$product = mysqli_fetch_assoc(mysqli_query($conn, $query));
						//use php's extract() function to get all keys of an associative array and assign
						//them to variables with the same name
						extract($product);
						$subtotal = $price * $quantity;
						$total += $subtotal; ?>

						<tr>
							<td>
								<img src="<?= $img_path; ?>" width="25">
								<?= $name; ?>
							</td>
							<td>
								<?= $price; ?>
							</td>
							<td>
								<?= $quantity; ?>
							</td>
							<td>
								<?= $subtotal; ?>
							</td>
						</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="3">
							<strong>Total: PhP <?= number_format($total, 2) ?></strong>
						</td>
						<td>
							<a href="../controllers/checkout.php" class="btn btn-primary">Checkout</a>
						</td>
					</tr>
				</tbody>
			</table>
		<?php else :
			echo "<h2>No items in cart.</h2>";
		endif;
	}
	require "./layouts/app.php";
}