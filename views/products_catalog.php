<?php

session_start();

//allow access to the catalog when there's nobody logged in or if non-admin user is logged in
if(!isset($_SESSION['isAdmin']) || (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 0)){
	$title = 'Catalog';
	function get_content(){
		require "../controllers/connection.php";
		$queryProducts = "SELECT products.*, categories.name AS category_name FROM products JOIN categories ON (categories.id = products.category_id)";
		$products = mysqli_query($conn, $queryProducts) or die(mysqli_error($conn)); ?>
		<div class="row">
			<?php foreach($products as $product) : ?>
				<div class="col-3 p-4">
					<div style="width: 50%;" class="mx-auto">
						<img src="<?= $product['img_path']; ?>" class="card-img-top" width="100%">
					</div>

					<div class="card-body">
						<h4 class="card-title"><?php $product['name']; ?></h4>
						<p class="card-text"><?= $product['price']; ?></p>
						<p class="card-text"><?= $product['description']; ?></p>
						<input class="quantity" type="number" min="1" name="quantity">
						<button type="button" class="addToCart" data-id="<?= $product['id'] ?>" disabled>
							Add to cart
						</button>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<script type="text/javascript" src="../assets/js/addToCart.js"></script>
		<?php }
		require "./layouts/app.php";
	}else{
		header('location: ./products_dashboard.php');
} ?>

	