<?php

session_start();

if(isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1){
	$title = "Admin Products Dashboard";

	function get_content(){
		require "../controllers/connection.php";
		//query the categories
		$queryCategories = "SELECT * FROM categories";
		$categories = mysqli_query($conn, $queryCategories) or die(mysqli_error($conn));

		//query the products table joining it with the categories table so that we can access the category name from the same result
		//due to duplicate column names, we had to assign an alias using the AS SQL keyword to the name column in the categories table while all columns were selected from the products table
		$queryProducts = "SELECT products.*, categories.name AS category_name FROM products JOIN categories ON (categories.id = products.category_id)";
		$products = mysqli_query($conn, $queryProducts) or die(mysqli_error($conn)); ?>

		<!-- edit product modal -->
		<div class="modal" id="editModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- modal header -->
					<div class="modal-header">
						<h4 class="modal-title">Edit Product</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- modal body -->
					<div class="modal-body" id="editModalBody"></div>
						<!-- this is left blank becaue the content for this modal is generated dynamilcally from the edit_form.php controller and inserted here via the editProduct.js's fetch function -->
				</div>
			</div>
		</div>
		<!-- end of edit product modal -->

		<!-- start of delete modal -->
		<div class="modal" id="delModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- modal header -->
					<div class="modal-header">
						<h4 class="modal-title" id="deleteModalTitle">Delete Product</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- modal body -->
					<div class="modal-body" id="deleteModalBody">
						
					</div>

					<!-- modal footer -->
					<div class="modal-footer">
						<a class="btn btn-danger" id="modalDeleteBtn">Delete</a>
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- end of delete modal -->

		<!-- start of row that will contain an add category collabsible card and an add product collapsible card -->
				<div class="row">
					<div class="col-8 offset-2">
						<div id="accordion">
							<!-- card for add category form -->
							<div class="card">
								<div class="card-header">
									<a class="card-link h3" data-toggle="collapse" href="#addCategory">
										Add Category
									</a>
								</div>

								<div id="addCategory" class="collapse" data-parent="#accordion">
									<div class="card-body">
										<form>
											<div class="form-group">
												<label for="name">Category name: </label>

												<input class="form-control" type="text" id="catName" name="description">
											</div>
										</form>
										<button class="btn btn-success" id="addCatBtn">Add Category</button>
									</div>	
								</div>
							</div>
							<!-- end of add category card -->

							<!-- add product card -->
							<div class="card">
								<div class="card">
									<div class="card-header">
										<a class="card-link h3" data-toggle="collapse" href="#addProduct">
											Add Product
										</a>
									</div>

									<div id="addProduct" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<form method="post" action="../controllers/add_product.php" enctype="multipart/form-data">
												<div class="form-group">
													<label for="name">Name:</label>
													<input class="form-control" type="text" id="name" name="name">
												</div>
												<div class="form-group">
													<label for="description">Description:</label>
													<input class="form-control" type="text" id="description" name="description">
												</div>
												<div class="form-group">
													<label for="price">Price:</label>
													<input class="form-control" type="number" id="price" name="price">
												</div>
												<!-- category select dropdown -->
												<div class="form-group">
													<label for="category_id">Category:</label>
													<select class="form-control" id="category_id" name="category">
														<option>Select a category:</option>
														<?php foreach ($categories as $category) : ?>
															<option value="<?= $category['id']; ?>">
																<?= $category['name']; ?>
															</option>
														<?php endforeach; ?>
													</select>
												</div>
												<div class="form-group">
													<label for="image">Image:</label>
													<input class="form-control" type="file" id="image" name="image">
												</div>
												<button class="btn btn-success">Add Product</button>
											</form>
										</div>
									</div>
								</div>
							</div>
							<!-- end of add product card -->
						</div>
						<!-- end of accordion -->
					</div>
				</div>
				<!-- end of add category and add product row -->

				<!-- start of products table view -->
				<div class="row">
					<table class="table">
					  <thead>
					    <tr>
					      <th scope="col">Image</th>
					      <th scope="col">Name</th>
					      <th scope="col">Description</th>
					      <th scope="col">Price</th>
					      <th scope="col">Category</th>
					      <th scope="col">Actions</th>
					    </tr>
					  </thead>
					  <tbody>
					  	<?php foreach($products as $product) : ?>
					    <tr>
					      <td style="width:10%;">
					      	<img class="img-fluid" src="<?= $product['img_path']; ?>">
					      </td>
					      <td><?= $product['name']; ?></td>
					      <td><?= $product['description']; ?></td>
					      <td><?= $product['price']; ?></td>
					      <td><?= $product['category_name']; ?></td>
					      <!-- we can use HTML data attributes to pass around variables -->
					      <td data-id="<?= $product['id']; ?>" data-name="<?= $product['name']?>">
					      	<button class="btn btn-warning editBtn" data-toggle="modal" data-target="#editModal">Edit</button>
					      	<button class="btn btn-danger delBtn" data-toggle="modal" data-target="#delModal">Delete</button>
					      </td>
					    </tr>
						<?php endforeach;?>
					  </tbody>
					</table>
				</div>
				<!-- start of products table view -->				
				<script type="text/javascript" src="../assets/js/add_cat.js"></script>				
				<script type="text/javascript" src="../assets/js/editProduct.js"></script>
				<script type="text/javascript" src="../assets/js/deleteProduct.js"></script>
	<?php };

	require "./layouts/app.php";
}else{
	header('location: ./products_catalog.php');
}
?>