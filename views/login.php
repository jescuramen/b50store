<?php

$title = "Login";

function get_content(){ ?>
	<div class="row">
		<div class="col-8 offset-2">
			<h1>Login Form</h1>

			<form method="POST" action="../controllers/login.php">
				<div class="form-group row">
					<label for="username" class="col-3 text-right">Username:</label>
					<input type="text" class="form-control col-9" id="username" name="username">
					<span id="usernameError" class="col-9 offset-3"></span>
				</div>
				<!-- password input -->
				<div class="form-group row">
					<label for="pword1" class="col-3 text-right">Password:</label>
					<input type="password" class="form-control col-9" id="pword1" name="password">
					<span id="pword1Error" class="col-9 offset-3"></span>
				</div>
				<!-- form submission button -->
				<button type="submit" id="submitBtn" class="btn btn-primary">Login</button>
			</form>
		</div>  
	</div>
<?php }; 

require_once "./layouts/app.php";
?>