const editModalBody = document.querySelector('#editModalBody');

document.querySelectorAll('.editBtn').forEach((btn)=>{
    btn.addEventListener("click", ()=>{
        let id = btn.parentElement.getAttribute('data-id');
        //console.log(id);
        fetch('../../controllers/edit_form.php?id='+id)
        .then((res)=>{
            return res.text();
        })
        .then((data)=>{
            //console.log(editModalBody);
            editModalBody.innerHTML = data;
        });
    });
})