//make a collection of all delete buttons
const deleteModalBtns = document.querySelectorAll('.delBtn');

deleteModalBtns.forEach((btn)=>{
	//attach an event listener to each delete button that will listen for a click event 
	btn.addEventListener("click", ()=>{
		//get the id of the product to be deleted
		let id = btn.parentElement.getAttribute('data-id');
		//get the name of the product to be deleted
		let name = btn.parentElement.getAttribute('data-name');
		//select the delete modal body element
		let delModalBody = document.querySelector('#deleteModalBody');
		//select the delete modal title element
		let deleteModalTitle = document.querySelector('#deleteModalTitle');
		//select the delete button element of THE MODAL
		let deleteModalBtn = document.querySelector('#modalDeleteBtn');

		//in the delete modal body, append the name of the product to be deleted to 
		//the hardcoded message in HTML
		delModalBody.innerHTML = `Are you sure you want to delete ${name}`;

		//in the delete modal title, output the string 'Delete' + <productName>
		deleteModalTitle.innerHTML = `Delete ${name}`;

		deleteModalBtn.href=`../controllers/delete_product.php?id=${id}`;

	});
});