const addToCartBtns = document.querySelectorAll('.addToCart');
const addToCartInputs = document.querySelectorAll('.quantity');

//for each quantity input field, attach an input event listener that
//will enable the add to cart button when the input field is not blank.
addToCartInputs.forEach((input)=>{
	input.addEventListener("input", function(){
		if(input.value != ""){
			this.nextElementSibling.disabled = false;
		}else{
			this.nextElementSibling.disabled = true;
		}
	})
})

//implement the add to cart functionality for each add to cart button
addToCartBtns.forEach(function(button){
	button.addEventListener("click", function(){
		//select the cartCount element in the navbar
		const cartCount = document.querySelector('#cartCount');

		//get the desired quantity for the product whose add to cart button was clicked
		let quantityField = this.previousElementSibling;
		let quantity = quantityField.value;
		//get the id of the product to be added to the cart
		let id = this.getAttribute('data-id');
		//console.log(`ID: ${id}, quantity: ${quantity}`);

		//reset the input field value after clicking the add to cart
		quantityField.value = "";
		//disable the add to cart button once again
		button.disabled = true;

		//append the id and quantity of the product to be added to the cart to the FormData object
		let data = new FormData;
		data.append('id', id);
		data.append('quantity', quantity);

		//send the above form data to the add_to_cart controller via POST method
		fetch('../controllers/add_to_cart.php', {
			method: 'POST',
			body: data
		})
		.then(()=>{
			//if the cart counter in the nav bar is NOT empty
			if(cartCount.innerHTML != ''){
				console.log(cartCount.innerHTML);
				//add the new quantity to the existing quantity
				cartCount.innerHTML = parseInt(cartCount.innerHTML) + parseInt(quantity);
			}else{
				//set the cart counter to the new quantity
				cartCount.innerHTML = parseInt(quantity);
			}
		})
	})
})